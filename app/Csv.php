<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 19.02.2018
 * Time: 18:07
 */

namespace App;


class Csv
{
    public $pathCsv = null;

    public function getCsvFile()
    {
        if ($handle = fopen($this->pathCsv, "r")) {
            while (!feof($handle)) {
                $dataFromCSV[] = fgets($handle);
            }
            fclose($handle);

            return $dataFromCSV;
        } else return false;
    }

    public function saveCsv($tmpName)
    {
        @mkdir("inputFilesCsv", 0777);
        $nameFile = substr(md5(microtime() . rand(0, 9999)), 0, 20) . ".csv";
        copy($tmpName, "inputFilesCsv/" . basename($nameFile));
        return 'inputFilesCsv/' . $nameFile;
    }

    public function setCsvFile(Array $data)
    {
        $handle = fopen($this->pathCsv, "a");
        foreach ($data as $value) {
            $value = trim($value);
            fwrite($handle, $value . PHP_EOL);
        }
        fclose($handle);
    }
}