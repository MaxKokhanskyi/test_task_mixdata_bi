totalPages = 1;
pathFile = undefined;

function initRenderResult() {
    if (document.getElementById('blockResult'))
        document.getElementById('blockResult').parentNode.removeChild(document.getElementById('blockResult'));

    var resultWrap = document.getElementById('result');
    var htmlBlockResult = document.createElement('div');

    htmlBlockResult.id = "blockResult";
    resultWrap.insertBefore(htmlBlockResult, resultWrap.lastChild);
}

function renderLink(link) {
    if (document.querySelector('a'))
        document.querySelector('a').parentNode.removeChild(document.querySelector('a'));
    var resultWrap = document.getElementById('result');
    var a = document.createElement('a');
    a.textContent = 'Link downloads';
    a.href = link;
    resultWrap.insertBefore(a, resultWrap.firstChild);
}

function renderPagination(pathFile, totalPages) {
    var blockResult = document.getElementById('blockResult');
    var nav = document.createElement('nav');
    nav.id = "nave";
    var htmlNav = '<ul class="pagination">';

    for (var i = 1; i <= totalPages; i++) {
        onlyLi = '<li data-page="' + i + '" class="li-page page-item">';
        onlyLi += '<span class="page-link">' + i + '</span>';
        onlyLi += '</li>';
        htmlNav += onlyLi;
    }
    htmlNav += '</ul>';
    nav.innerHTML = htmlNav;
    blockResult.insertBefore(nav, blockResult.lastChild);

    var listLi = document.getElementsByClassName('li-page');
    for (var i = 0; i < listLi.length; i++) {
        listLi[i].addEventListener('click', function () {
            var dataPage = this.getAttribute('data-page');
            getPagination(dataPage, pathFile);
        }, false)
    }
}

function getPagination(numberPage, file) {
    var url = "?task=getResult&nameFile=" + file + "&numberPage=" + numberPage;
    if (!document.getElementById('page-' + numberPage))
        var data = ajax(url);
    renderResult(data, numberPage);
}

function renderResult(data, page) {
    for (var i = 0; i < totalPages; i++) {
        if (document.getElementById('page-' + i))
            document.getElementById('page-' + i).style.display = 'none';
    }
    if (document.getElementById('page-' + page)) {
        document.getElementById('page-' + page).style.display = 'block'
    } else {
        var blockResult = document.getElementById('blockResult');
        var htmlListResult = document.createElement('div');
        htmlListResult.id = "page-" + page;
        blockResult.insertBefore(htmlListResult, blockResult.lastChild);

        var divPage = document.getElementById("page-" + page);

        var htmlListResult = document.createElement('div');
        htmlListResult.id = "listResult-" + page;
        data.forEach(function (item, i) {
            htmlListResult.innerHTML += item + '<br>';
        });
        divPage.insertBefore(htmlListResult, divPage.lastChild);
    }
}

purposeUseSearch = document.getElementById('typeInputData-text');
purposeUseSearch.onchange = function () {
    document.getElementById('text-block').style.display = "block";
    document.getElementById('file-block').style.display = "none";
};

purposeUseSearch = document.getElementById('typeInputData-file');
purposeUseSearch.onchange = function () {
    document.getElementById('text-block').style.display = "none";
    document.getElementById('file-block').style.display = "block";
};
purposeUseSearch = document.getElementById('purposeUseUniqueness');
purposeUseSearch.onchange = function () {
    document.getElementById('wrapSearch').style.display = "none";
};
purposeUseSearch = document.getElementById('purposeUseSearch');
purposeUseSearch.onchange = function () {
    document.getElementById('wrapSearch').style.display = "block";
};
radioMatchesPartial = document.getElementById('matchesPartial');
radioMatchesPartial.onchange = function () {
    addInputMatches();
};
matchesComplete = document.getElementById('matchesComplete');
matchesComplete.onchange = function () {
    document.getElementById('wrapCountMatchesPartial').parentNode.removeChild(document.getElementById('wrapCountMatchesPartial'));
};

function addInputMatches() {
    radioMatchesPartial = document.getElementById('wrapMatches');
    var html = document.createElement('div');
    html.className = "form-group";
    html.id = "wrapCountMatchesPartial";
    html.innerHTML = '<label for=""countMatchesPartial">Difference</label>\n' +
        '<input type="number" min="0" required value="0" name="countMatchesPartial" class="form-control" id="countMatchesPartial">';
    radioMatchesPartial.insertBefore(html, radioMatchesPartial.lastChild);
}

var form = document.querySelector("form");
form.addEventListener('submit', function (ev) {
    if (document.getElementById('text').value.match(/\n/g) != null && document.getElementById('text').value.match(/\n/g).length > 100000) {
        alert('Phrases more than 10,000');
    } else {
        var url = "?task=go";
        var data;
        oData = new FormData(form);
        var oReq = new XMLHttpRequest();
        oReq.open("POST", url, false);
        oReq.onload = function (oEvent) {
            if (oReq.status == 200) {
                console.log(oReq);
                data = JSON.parse(oReq.response);
                if (data['mess'] !== undefined) {
                    alert(data['mess']);
                    if (document.getElementById('blockResult'))
                        document.getElementById('blockResult').parentNode.removeChild(document.getElementById('blockResult'));
                } else {
                    totalPages = data['totalPages'];
                    pathFile = data['pathFile'];
                    initRenderResult();
                    if (totalPages > 1) {
                        renderPagination(data['pathFile'], data['totalPages']);
                    }
                    renderLink(data['pathFile']);
                    renderResult(data['data'], 1);
                }
            } else {
                console.log("Error " + oReq);
            }
        };
        oReq.send(oData);
    }
    ev.preventDefault();
}, false);

function ajax(url) {
    var data;
    var oReq = new XMLHttpRequest();
    oReq.open("POST", url, false);
    oReq.onload = function (oEvent) {
        if (oReq.status == 200) {
            console.log(oReq);
            data = JSON.parse(oReq.response);
            totalPages = data['totalPages'];
            pathFile = data['pathFile'];
        } else {
            console.log("Error " + oReq);
        }
    };
    oReq.send(null);
    return data['data'];
}

function getXmlHttp() {
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}