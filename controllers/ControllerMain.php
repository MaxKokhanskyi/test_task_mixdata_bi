<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 19.02.2018
 * Time: 18:09
 */

namespace Controllers;

require_once 'models/SearchModel.php';
require_once 'app/Csv.php';

use App\Csv;
use Models\SearchModel;


class ControllerMain
{
    private $search_model;
    private $csv;

    public function __construct()
    {
        $this->search_model = new SearchModel();
        $this->csv = new CSV();
    }

    public function index($request, $files)
    {

        if (!empty($request['text']) && $request['typeInputData'] == 'text') {
            $data = explode(PHP_EOL, $request['text']);
        } else if (!empty($files['file']['tmp_name']) && $request['typeInputData'] == 'file') {
            $this->csv->pathCsv = $this->csv->saveCsv($files['file']['tmp_name']);
            $data = $this->csv->getCsvFile();
            if ($data !== false)
                unlink($this->csv->pathCsv);
            else {
                json_encode(['mess' => 'Not file']);
                return;
            }
        } else {
            echo json_encode(['mess' => 'Selected file or text']);
            return;
        }

        if ($request['purposeUse'] == 'search') {
            $string = strip_tags(trim($request['searchInput']));
            if (!empty($request['countMatchesPartial']))
                $countFind = $request['countMatchesPartial'];
            else $countFind = 0;
            $pathFileOut = $this->search_model->search($string, $data, $countFind);
            if ($pathFileOut !== false) {
                $result = $this->search_model->getResultSearch($pathFileOut);
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                return;
            } else {
                echo json_encode(['mess' => 'Not found concurrences'], JSON_UNESCAPED_UNICODE);
                return;
            }
        } else if ($request['purposeUse'] == 'uniqueness') {
            $pathFileOut = $this->search_model->uniqueness($data);
            echo json_encode($this->search_model->getResultSearch($pathFileOut), JSON_UNESCAPED_UNICODE);
            return;
        } else
            echo json_encode(['mess' => 'Error; Selected purpose use'], JSON_UNESCAPED_UNICODE);
        return;
    }

    public function getResult($pathFile, $numberPage)
    {
        $pathFile = trim($pathFile);
        $numberPage = (int)trim($numberPage);
        echo json_encode($this->search_model->getResultSearch($pathFile, $numberPage), JSON_UNESCAPED_UNICODE);
    }
}