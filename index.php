<?php

require_once 'controllers/ControllerMain.php';

use Controllers\ControllerMain;
$controller = new ControllerMain();

if (isset($_GET['task']) && $_GET['task'] == 'go'){
    $controller->index($_REQUEST, $_FILES);
}

if (isset($_GET['task']) && $_GET['task'] == 'getResult' && !empty($_GET['nameFile'])){
    $controller->getResult($_GET['nameFile'], $_GET['numberPage']);
}

if (!isset($_GET['task']))
    require 'view/searchView.php';





