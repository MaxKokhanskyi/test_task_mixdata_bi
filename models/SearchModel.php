<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 19.02.2018
 * Time: 18:11
 */

namespace Models;

require_once 'app/Csv.php';

use App\Csv;

class SearchModel
{
    public function getResultSearch(string $pathFile, int $numberPage = 1)
    {
        $data = @file($pathFile);
        $count = count($data);
        $maxRowOnPages = 20;

        $totalPages = ceil($count / $maxRowOnPages);

        if ($numberPage > $totalPages)
            $numberPage = 1;

        $first = $numberPage * $maxRowOnPages - $maxRowOnPages;
        $last = ($numberPage * $maxRowOnPages) - 1;

        $outData = [];
        for ($i = $first; $i <= $last; $i++) {
            if ($i < $count)
                $outData['data'][] = $data[$i];
        };
        $outData['totalPages'] = $totalPages;
        $outData['numberPage'] = $numberPage;
        $outData['pathFile'] = $pathFile;
        return $outData;
    }

    public function search($string, array $data, int $countFind = 0)
    {
        $result = array();
        preg_match_all('~(\w{2,})~ui', $string, $wordArSearchString);
        foreach ($data as $phrase) {
            $countFound = 0;
            $wordArPhrase = array();
            preg_match_all('~(\w{2,})~ui', $phrase, $wordArPhrase);
            if ($countFind == 0 && count($wordArPhrase[1]) != count($wordArSearchString[1])) continue;
            if ((count($wordArPhrase[1]) + $countFind) < count($wordArSearchString[1])) ;
            foreach ($wordArSearchString[1] as $word) {
                if (preg_match('/\b' . $word . '\b/ui', $phrase))
                    $countFound++;
            }
            if ($countFound >= (count($wordArPhrase[1]) - $countFind) && $countFound >= 1)
                $result[] = $phrase;
        }

        if (count($result) != 0) {
            $pathFile = 'filesCsv/' . substr(md5(microtime() . rand(0, 9999)), 0, 20) . ".csv";
            $csv = new Csv();
            $csv->pathCsv = $pathFile;
            $csv->setCsvFile($result);
            return $pathFile;
        } else
            return false;
    }

    public function uniqueness($data)
    {
        $dataUnique = array_unique($data);
        $pathFile = 'filesCsv/' . substr(md5(microtime() . rand(0, 9999)), 0, 20) . ".csv";
        $csv = new Csv();
        $csv->pathCsv = $pathFile;
        $csv->setCsvFile($dataUnique);
        return $pathFile;
    }
}