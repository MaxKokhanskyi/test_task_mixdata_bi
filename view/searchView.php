<html>
<head>
    <title>Search</title>
    <link rel="stylesheet" href="assets/style.css">
    <style>
        #wrapSearch, #text-block {
            display: none;
        }
    </style>
</head>
<body>
<div class="wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <form enctype="multipart/form-data">
                    <span>Choose the method of data transfer</span>
                    <br>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="typeInputData" id="typeInputData-file"
                               value="file" checked>
                        <label class="form-check-label" for="typeInputData-file">File</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="typeInputData" id="typeInputData-text"
                               value="text">
                        <label class="form-check-label" for="matchesPartial">Text</label>
                    </div>
                    <hr>
                    <div class="form-group" id="file-block">
                        <label for="file">File with phrases</label>
                        <input type="file" name="file" class="form-control" id="file" accept=".csv">
                        <small id="fileHelp" class="form-text text-muted">
                            Formats: .csv; One line is one phrase;
                        </small>
                    </div>
                    <div class="form-group" id="text-block">
                        <label for="text">Enter phrases</label>
                        <textarea name="text" id="text" class="form-control" cols="30" rows="10"></textarea>
                        <small id="fileHelp" class="form-text text-muted">
                            One line is one phrase; 10,000 phrases are maximum;
                        </small>
                    </div>
                    <div class="wrapPurposeUse">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="purposeUse" id="purposeUseUniqueness"
                                   value="uniqueness" checked>
                            <label class="form-check-label" for="purposeUseUniqueness">
                                Uniqueness
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="purposeUse" id="purposeUseSearch"
                                   value="search">
                            <label class="form-check-label" for="purposeUseSearch">
                                Search
                            </label>
                        </div>
                    </div>
                    <br>
                    <div id="wrapSearch" class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="searchInput">Search query</label>
                                <input type="text" name="searchInput" class="form-control" id="searchInput">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div id="wrapMatches">
                                <p>Coincidences</p>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="matches" id="matchesComplete"
                                           value="complete" checked>
                                    <label class="form-check-label" for="matchesComplete">
                                        Full
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="matches" id="matchesPartial"
                                           value="partial">
                                    <label class="form-check-label" for="matchesPartial">
                                        Partial
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="col-lg-6">
                <div id="result">
                    <div id="blockResult">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/main.js"></script>
</body>
</html>